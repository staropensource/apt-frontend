echo "Welcome to the apt-frontend setup."
echo "Starting install in 5s..."
sleep 5s
rm -rf /etc/apt-frontend/
mkdir /tmp/apt-frontend/ 
cd /tmp/apt-frontend/
echo "Downloading frontend.py..."
wget "https://gitlab.com/staropensource/apt-frontend/-/raw/main/frontend.py" -q
echo "Downloading startup.py..."
wget "https://gitlab.com/staropensource/apt-frontend/-/raw/main/startup.py" -q
echo "Downloading about.py..."
wget "https://gitlab.com/staropensource/apt-frontend/-/raw/main/about.py" -q
echo "Downloading autopurge.py..."
wget "https://gitlab.com/staropensource/apt-frontend/-/raw/main/autopurge.py" -q
echo "Downloading autoremove.py..."
wget "https://gitlab.com/staropensource/apt-frontend/-/raw/main/autoremove.py" -q
echo "Downloading install.py..."
wget "https://gitlab.com/staropensource/apt-frontend/-/raw/main/install.py" -q
echo "Downloading logo.txt..."
wget "https://gitlab.com/staropensource/apt-frontend/-/raw/main/logo.txt" -q
echo "Downloading purge.py..."
wget "https://gitlab.com/staropensource/apt-frontend/-/raw/main/purge.py" -q
echo "Downloading remove.py..."
wget "https://gitlab.com/staropensource/apt-frontend/-/raw/main/remove.py" -q
echo "Downloading search.py..."
wget "https://gitlab.com/staropensource/apt-frontend/-/raw/main/search.py" -q
echo "Downloading update.py..."
wget "https://gitlab.com/staropensource/apt-frontend/-/raw/main/update.py" -q
echo "Downloading upgrade.py..."
wget "https://gitlab.com/staropensource/apt-frontend/-/raw/main/upgrade.py" -q
sudo -u root mkdir /etc/apt-frontend/
sudo -u root mv * /etc/apt-frontend/
rm -rf /tmp/apt-frontend/
sudo -u root echo "python3 /etc/apt-frontend/frontend.py" > /usr/bin/apt-frontend
sudo -u root chmod +x /usr/bin/apt-frontend
echo "Installation finished."
echo "Try executing 'apt-frontend' now!"
