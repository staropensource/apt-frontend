**WARNING**
apt-frontend-old is deprecated and will not get any updates! Please move to [apt-frontend](https://gitlab.com/staropensource/apt-frontend/)

# What is apt-frontend
apt-frontend is a program made for GNU/Linux computers that have apt installed (like Debian, Ubuntu, Linux Mint, etc.). It makes managing packages easier for both beginners and professionals.

# How can I install/update it?
Execute this: **sudo -u root apt install curl bash wget && curl -sSL https://gitlab.com/staropensource/apt-frontend-old/-/raw/main/INSTALL.sh | sudo -u root bash**

# How to build it?
You don't need to build apt-frontend because they're all python3 scripts.

# What language is apt-frontend written in?
Read the question above.

# Does this project have a license?
Yes, we have our own license. You can find it in [LICENSE.md](https://gitlab.com/staropensource/apt-frontend-old/-/blob/main/LICENSE.md)
