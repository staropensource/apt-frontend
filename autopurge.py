import os,sys

print("Auto purge")
print("")
print("Do you want to auto purge all installed packages that aren't needed anymore?")
autopurge = input("[Y/n]: ")
if autopurge == "y" or autopurge == "Y" or autopurge == "Yes" or autopurge == "yes":
	print("Auto purging packages...\n")
	os.system("apt -y autopurge")
	print("\nAuto purged packages. See for errors or warnings above.")
	enter = input("Press enter... ")
	sys.exit()
elif autopurge == "n" or autopurge == "N" or autopurge == "No" or autopurge == "no":
	sys.exit()
else:
	print("Error: Wrong validation")
	print("       Please use 'y', 'Y', 'Yes', 'yes', 'n', 'N', 'No', or 'no'.")
	enter = input("\nPress enter... ")
	sys.exit()
