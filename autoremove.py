import os,sys

print("Auto remove")
print("")
print("Do you want to auto remove all installed packages that aren't needed anymore?")
autoremove = input("[Y/n]: ")
if autoremove == "y" or autoremove == "Y" or autoremove == "Yes" or autoremove == "yes":
	print("Auto removing packages...\n")
	os.system("apt -y autoremove")
	print("\nAuto removed packages. See for errors and warnings above.")
	enter = input("Press enter... ")
	sys.exit()
elif autoremove == "n" or autoremove == "N" or autoremove == "No" or autoremove == "no":
	sys.exit()
else:
	print("Error: Wrong validation")
	print("       Please use 'y', 'Y', 'Yes', 'yes', 'n', 'N', 'No', or 'no'.")
	enter = input("\nPress enter... ")
	sys.exit()
