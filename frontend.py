###############################################################################
#                                                                             #
#          _    ____ _____    _____                _                 _        #
#         / \  |  _ \_   _|  |  ___| __ ___  _ __ | |_ ___ _ __   __| |       #
#        / _ \ | |_) || |    | |_ | '__/ _ \| '_ \| __/ _ \ '_ \ / _` |       #
#       / ___ \|  __/ | |    |  _|| | | (_) | | | | ||  __/ | | | (_| |       #
#      /_/   \_\_|    |_|    |_|  |_|  \___/|_| |_|\__\___|_| |_|\__,_|       #
#                                                                             #
#                                                                             #
###############################################################################
#   apt-frontend is a pack of python scripts that automate the life of any    #
#   linux beginner or professional user. apt-frontend provides a easy to      #
#   use interface that has all normal options. Of course you can still        #
#   (like ever) manage apt from the command line. We hope that apt-frontend   #
#   helps linux users. This tool was written on the 27.05.2021.               #
###############################################################################
#         This tool was originally created by JeremyStar/JeremyStarTM.        #
#          He used Ubuntu 20.04.2 LTS with KDE Plasma 5.21 back then.         #
###############################################################################
#      Do you need help or have issues with apt-frontend? Make a issue on     #
#        our GitLab project site where also new releases are published.       #
###############################################################################
#      Things that were we changed the "normal" code to a different code      #
#   (If you find lines that don't match, please make a new issue to fix it)   #
#                                                                             #
# startup.py Line 38/44: Changing from "apt" to "apt-get" to hide the message #
#                 "apt does not have a stable cli interface, ..."             #
###############################################################################

import os,sys

os.system("clear")
os.system("cat /etc/apt-frontend/logo.txt")
print("apt-frontend made by JeremyStar/JeremyStarTM")
print("https://links-jeremystartm.netlify.app")
print("\n\n")
print("ID | Action")
print("00 | Quit")
print("01 | Install")
print("02 | Remove")
print("03 | Search")
print("04 | Update (when needed)")
print("05 | Upgrade")
print("06 | Purge")
print("07 | Auto remove")
print("08 | Auto purge")
print("99 | About apt-frontend")
action0 = input("ID: ")
if action0 == "00" or action0 == "0":
	sys.exit(0)
elif action0 == "01" or action0 == "1":
	os.system("python3 /etc/apt-frontend/install.py")
	os.system("python3 /etc/apt-frontend/frontend.py")
	sys.exit()
elif action0 == "02" or action0 == "2":
	os.system("python3 /etc/apt-frontend/remove.py")
	os.system("python3 /etc/apt-frontend/frontend.py")
	sys.exit()
elif action0 == "03" or action0 == "3":
	os.system("python3 /etc/apt-frontend/search.py")
	os.system("python3 /etc/apt-frontend/frontend.py")
	sys.exit()
elif action0 == "04" or action0 == "4":
	os.system("python3 /etc/apt-frontend/update.py")
	os.system("python3 /etc/apt-frontend/frontend.py")
	sys.exit()
elif action0 == "05" or action0 == "5":
	os.system("python3 /etc/apt-frontend/upgrade.py")
	os.system("python3 /etc/apt-frontend/frontend.py")
	sys.exit()
elif action0 == "06" or action0 == "6":
	os.system("python3 /etc/apt-frontend/purge.py")
	os.system("python3 /etc/apt-frontend/frontend.py")
	sys.exit()
elif action0 == "07" or action0 == "7":
	os.system("python3 /etc/apt-frontend/autoremove.py")
	os.system("python3 /etc/apt-frontend/frontend.py")
	sys.exit()
elif action0 == "08" or action0 == "8":
	os.system("python3 /etc/apt-frontend/autopurge.py")
	os.system("python3 /etc/apt-frontend/frontend.py")
	sys.exit()
elif action0 == "99":
	os.system("python3 /etc/apt-frontend/about.py")
	os.system("python3 /etc/apt-frontend/frontend.py")
	sys.exit()
else:
	print("Error: ID wasn't found.")
	print("       Please try entering a valid ID.")
	print("")
	enter = input("Press enter... ")
	os.system("python3 /etc/apt-frontend/frontend.py")
	sys.exit()
