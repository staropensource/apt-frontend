###############################################################################
#                                                                             #
#          _    ____ _____    _____                _                 _        #
#         / \  |  _ \_   _|  |  ___| __ ___  _ __ | |_ ___ _ __   __| |       #
#        / _ \ | |_) || |    | |_ | '__/ _ \| '_ \| __/ _ \ '_ \ / _` |       #
#       / ___ \|  __/ | |    |  _|| | | (_) | | | | ||  __/ | | | (_| |       #
#      /_/   \_\_|    |_|    |_|  |_|  \___/|_| |_|\__\___|_| |_|\__,_|       #
#                                                                             #
#                                                                             #
###############################################################################
#   apt-frontend is a pack of python scripts that automate the life of any    #
#   linux beginner or professional user. apt-frontend provides a easy to      #
#   use interface that has all normal options. Of course you can still        #
#   (like ever) manage apt from the command line. We hope that apt-frontend   #
#   helps linux users. This tool was written on the 27.05.2021.               #
###############################################################################
#         This tool was originally created by JeremyStar/JeremyStarTM.        #
#          He used Ubuntu 20.04.2 LTS with KDE Plasma 5.21 back then.         #
###############################################################################
#      Do you need help or have issues with apt-frontend? Make a issue on     #
#        our GitLab project site where also new releases are published.       #
###############################################################################

import os,sys

os.system("clear")
print("Please wait, do not cancel apt-frontend, preparing execution in 3s...")
os.system("sleep 3s")
os.system("clear")
print("Please wait, do not cancel apt-frontend!")
print("[ DOING ] Updating apt repositories")
print("[WAITING] Updating apt, dpkg and python3")
print("[WAITING] Updating/installing apt-transport-https")
os.system("apt -qq update")
os.system("clear")
print("Please wait, do not cancel apt-frontend!")
print("[ READY ] Updating apt repositories")
print("[ DOING] Updating apt, dpkg and python3")
print("[WAITING] Updating/installing apt-transport-https")
os.system("apt-get -qq -y install apt dpkg python3 > /dev/null")
os.system("clear")
print("Please wait, do not cancel apt-frontend!")
print("[ READY ] Updating apt repositories")
print("[ READY ] Updating apt, dpkg and python3")
print("[ DOING ] Updating/installing apt-transport-https")
os.system("apt-get -qq -y install apt-transport-https > /dev/null")
os.system("clear")
print("apt-frontend has prepared itself for execution, starting in 3s...")
print("[ READY ] Updating apt repositories")
print("[ READY ] Updating apt, dpkg and python3")
print("[ READY ] Updating/installing apt-transport-https")
os.system("sleep 3s")
os.system("python3 /etc/apt-frontend/frontend.py")
