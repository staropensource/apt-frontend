import os,sys

print("Upgrade")
print("")
print("Do you want to update all installed packages with new updates?")
upgrade = input("[Y/n]: ")
if upgrade == "y" or upgrade == "Y" or upgrade == "Yes" or upgrade == "yes":
	print("Upgrading packages...\n")
	os.system("apt -y upgrade")
	print("\nUpgraded packages. See for errors and warnings above.")
	enter = input("Press enter... ")
	sys.exit()
elif upgrade == "n" or upgrade == "N" or upgrade == "No" or upgrade == "no":
	sys.exit()
else:
	print("Error: Wrong validation")
	print("       Please use 'y', 'Y', 'Yes', 'yes', 'n', 'N', 'No', or 'no'.")
	enter = input("\nPress enter... ")
	sys.exit()
